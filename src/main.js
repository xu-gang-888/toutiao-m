import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 导入全局样式
import './styles/index.css'
// 导入vant组件库
import Vant from 'vant'
import 'vant/lib/index.css'

// 导入 适配rem值 模块
import 'amfe-flexible'

import '@/utils/dayjs'
import * as filters from '@/filter/filters.js'
// 全局挂载vant组件库
Vue.use(Vant)
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.prototype.$toast.setDefaultOptions({ duration: 1000 })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
