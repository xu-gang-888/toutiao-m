// 请求模块
import axios from 'axios'
import store from '@/store'
import JOSNBigInt from 'json-bigint'

// 创建请求实例 避免在原型上挂载自己的东西
const request = axios.create({
  // 配置请求根路径
  baseURL: 'http://ttapi.research.itcast.cn/',
  // 自定义后端返回的原始数据data (JSON格式的数据)
  transformResponse: [
    function (data) {
      // axios默认在内部会这样处理后端返回的数据
      // return JSON.parse(data)
      try {
        // JOSNBigInt是一个处理大数字的插件 只有2个API  用法和JSON.parse 和JSON.stringify方法一样
        return JOSNBigInt.parse(data)
      } catch (error) {
        return data
      }
    }
  ]
})

// 添加请求拦截器配置请求头
// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    const { user } = store.state
    if (user && user.token) {
      config.headers.Authorization = `Bearer ${user.token}`
    }
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 向外暴露请求实例
export default request
