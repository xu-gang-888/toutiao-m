// 封装本地存储操作

// 设置本地数据
export const setItem = (key, value) => {
  value = JSON.stringify(value)
  window.localStorage.setItem(key, value)
}

// 获取本地数据
export const getItem = key => {
  const data = window.localStorage.getItem(key)
  try {
    return JSON.parse(data)
  } catch (error) {
    return data
  }
}

// 删除本地数据
export const removeItem = key => {
  window.localStorage.removeItem(key)
}
