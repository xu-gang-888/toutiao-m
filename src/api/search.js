import request from '../utils/request'

// 获取请求建议请求
export const getSearchSug = q => {
  return request({
    method: 'GET',
    url: '/app/v1_0/suggestion',
    params: {
      q
    }
  })
}

// 获取请求结果请求
export const getSearchRes = params => {
  return request({
    method: 'GET',
    url: '/app/v1_0/search',
    params
  })
}
