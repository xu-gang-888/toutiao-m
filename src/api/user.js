import request from '../utils/request'
// 用户登录请求
export const login = data => {
  return request({
    method: 'POST',
    url: '/app/v1_0/authorizations',
    data
  })
}

// 发送验证码请求
export const sendSms = mobile => {
  return request({
    method: 'GET',
    url: '/app/v1_0/sms/codes/' + mobile
  })
}

// 获取用户自己数据请求
export const getUserInfo = () => {
  return request({
    method: 'GET',
    url: '/app/v1_0/user'
  })
}

// 获取用户自己频道列表请求
export const getUserChannels = () => {
  return request({
    method: 'GET',
    url: '/app/v1_0/user/channels'
  })
}

// 添加关注请求
export const addFollow = id => {
  return request({
    method: 'POST',
    url: '/app/v1_0/user/followings',
    data: {
      target: id
    }
  })
}
// 取消关注请求
export const delFollow = id => {
  return request({
    method: 'DELETE',
    url: `/app/v1_0/user/followings/${id}`
  })
}

// 获取用户自己信息请求
export const getUserProfile = () => {
  return request({
    method: 'GET',
    url: '/app/v1_0/user/profile'
  })
}

// 修改用户自己信息请求
export const updateUserProfile = data => {
  return request({
    method: 'PATCH',
    url: '/app/v1_0/user/profile',
    data
  })
}

// 修改用户自己信息请求
export const updateUserPhoto = data => {
  return request({
    method: 'PATCH',
    url: '/app/v1_0/user/photo',
    data
  })
}
