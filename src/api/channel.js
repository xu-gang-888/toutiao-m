import request from '../utils/request'

// 请求全部频道列表请求
export const getAllChannel = () => {
  return request({
    method: 'GET',
    url: '/app/v1_0/channels'
  })
}

// 用户添加频道列表请求
export const addUserChannel = channel => {
  return request({
    method: 'PATCH',
    url: '/app/v1_0/user/channels',
    data: {
      channels: [channel]
    }
  })
}

// 用户删除频道列表请求id
export const delUserChannel = id => {
  return request({
    method: 'DELETE',
    url: '/app/v1_0/user/channels/' + id
  })
}
