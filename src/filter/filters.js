// 搜索文字变化过滤器
export const textRed = text => {
  const reg = new RegExp(text, 'gi')
  return text.replace(reg, `<span style='color:red;'>${text}</span>`)
}
