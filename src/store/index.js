import Vue from 'vue'
import Vuex from 'vuex'
// 导入自己封装的本地存储操作
import { getItem, setItem } from '../utils/storage'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // user: JSON.parse(window.localStorage.getItem('token'))
    user: getItem('token')
  },
  mutations: {
    setUser (state, data) {
      state.user = data
      // 存储到本地
      // window.localStorage.setItem('token', JSON.stringify(data))
      setItem('token', data)
    }
  },
  actions: {},
  modules: {}
})
