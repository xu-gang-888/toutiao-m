module.exports = {
  plugins: {
    // vue-lic内部已经配置了autofixer插件
    // autoprefixer: {
    //     browsers: ['Android >= 4.0', 'iOS >= 8'],
    // },
    'postcss-pxtorem': {
      // 我们自己的需要75，vant内置的是37.5  所以我们需要判断处理文件的时候是我们自己的文件还是vant文件
      rootValue ({ file }) {
        return file.indexOf('vant') !== -1 ? 37.5 : 75
      },
      // 数组的值是 要处理的css的属性 *代表所有
      propList: ['*'],
      exclude: 'github-markdown'
    }
  }
}
